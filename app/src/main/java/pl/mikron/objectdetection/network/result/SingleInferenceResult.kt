package pl.mikron.objectdetection.network.result

data class SingleInferenceResult(

    var durationInterpreter: Long = 0L,

    var durationMeasured: Long = 0
)
